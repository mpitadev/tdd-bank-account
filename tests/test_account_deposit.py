import unittest

from core.account import Account


class TestAccountDeposit(unittest.TestCase):
    def test_account_deposit(self) -> None:
        account = Account()
        list_money = [100, 200, 300, 400, 500]
        for money in list_money:
            balance = account.balance
            account.deposit(money)
            self.assertEqual(account.balance, balance + money)


if __name__ == '__main__':
    unittest.main()