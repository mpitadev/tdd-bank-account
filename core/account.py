class Account:
    def __init__(self):
        self.balance = 0
    
    def deposit(self, amount: int) -> None:
        self.balance += amount
