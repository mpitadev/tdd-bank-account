# Bank account specifications

* When a new account is created, the account balance is equal to 0
* When we make a deposit, the balance of this account becomes the value of the initial balance plus the money deposited
* When we withdraw money, the account balance becomes the value of the initial balance minus the money withdrawn
* If we try to withdraw more money than we have in the account, an exception will be thrown